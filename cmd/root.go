// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"os"
	"regexp"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/automac/cmdhelper"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgsvg"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "chart",
	Short: "A simple program to generate svg charts",
	Long: cmdhelper.MustFormatString(`
<p>
	chart is a simple program that takes a json object piped into stdin and
	generates an SVG chart. The input is in the following format:
</p>
<pre>
{
    "title": "Chart Title",
    "x_axis": {
        "label": "X Axis Title"
        "min": 0,
        "max": 10
    },
    "y_axis": {
        "label": "Y Axis Title"
        "min": -5,
        "max": 5
    },
    "data": [
        {
            "color": "#ff5555",
            "points": [
                { "x": 1.5, "y": 10.982 }
            ]
        }
    ]
}
</pre>
`),
	Run: func(cmd *cobra.Command, args []string) {

		rand.Seed(int64(2))

		o := &Options{}

		check(json.NewDecoder(os.Stdin).Decode(o))

		p, err := plot.New()
		check(err)

		p.Title.Text = o.Title
		p.X.Label.Text = o.XAxis.Label
		if o.XAxis.Min != nil {
			p.X.Min = *o.XAxis.Min
		}
		if o.XAxis.Max != nil {
			p.X.Max = *o.XAxis.Max
		}
		p.Y.Label.Text = o.YAxis.Label
		if o.YAxis.Min != nil {
			p.Y.Min = *o.YAxis.Min
		}
		if o.YAxis.Max != nil {
			p.Y.Max = *o.YAxis.Max
		}

		for _, s := range o.Data {
			check(addSeries(p, s))
		}

		c := vgsvg.New(4*vg.Inch, 4*vg.Inch)
		p.Draw(draw.New(c))
		_, err = c.WriteTo(os.Stdout)
		check(err)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
}

type Axis struct {
	Label string   `json:"label"`
	Min   *float64 `json:"min"`
	Max   *float64 `json:"max"`
}

type Point struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type Series struct {
	Points []Point `json:"points"`
	Color  Color   `json:"color"`
}

func (s *Series) Len() int {
	return len(s.Points)
}
func (s *Series) XY(i int) (x, y float64) {
	return s.Points[i].X, s.Points[i].Y
}

type Options struct {
	Title string    `json:"title"`
	XAxis Axis      `json:"x_axis"`
	YAxis Axis      `json:"y_axis"`
	Data  []*Series `json:"data"`
}

type Color string

func (c Color) RGBA() (r, g, b, a uint32) {

	matches := regexp.
		MustCompile(`^#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$`).
		FindStringSubmatch(string(c))

	if len(matches) == 0 {
		// warnf("invalid color value %s\n", c)
		return 0, 0, 0, math.MaxUint32
	}

	parts := []uint32{}

	for _, hex := range matches[1:] {
		dec, err := strconv.ParseInt(hex, 16, 32)
		if err != nil {
			panic(err)
		}
		parts = append(parts, uint32(dec*math.MaxUint32/255))
	}
	return uint32(parts[0]), uint32(parts[1]), uint32(parts[2]), math.MaxUint32
}

// https://github.com/gonum/plot/wiki/Example-plots

func check(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func warn(str ...interface{}) {
	fmt.Fprintln(os.Stderr, str...)
}
func warnf(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
}

func addSeries(p *plot.Plot, series *Series) error {
	line, points, err := plotter.NewLinePoints(series)
	if err != nil {
		return err
	}
	line.Color = series.Color
	points.Color = series.Color
	p.Add(line, points)
	return nil
}
